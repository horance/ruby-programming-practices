\begin{savequote}[45mm]
\ascii{Do the simplest thing that could possibly work.}
\qauthor{\ascii{- Kent Beck}}
\end{savequote}

\chapter{Object Model} 
\label{ch:object-model}

\section{Class and Object}

\begin{content}

\subsection{Object and Reference}

\ascii{Ruby}是一门纯粹的面向对象的语言，所有值都是对象，而且没有基本类型和对象类型的区分。

\begin{leftbar}
\begin{ruby}
class Point
  def initialize(x, y)
    @x, @y = x, y
  end

  def x()
    @x
  end

  def y()
    @y
  end
end

point = Point.new(0, 0)
\end{ruby}
\end{leftbar}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/object-reference}
  \caption{object reference}
  \label{fig:object-reference}
\end{figure}

\ascii{point}并不是对象本身，而是对象的引用。方法实参通过\ascii{pass-by-value}传递，而不是\ascii{pass-by-reference}，只不过传递的值恰好是对象的引用。

类也是一种对象，也必须通过引用对其进行访问。实际上，类名就是类对象的引用，只不过它恰好是一个常量而已。

\subsection{What's in the Object?}

\begin{leftbar}
\begin{ruby}
point.instance_variables         # [:@x, :@y]
point.methods.include? :x        # true
\end{ruby}
\end{leftbar}

虽然可以通过调用\ascii{point.methods}查询\ascii{point}对象的方法列表，但对象\ascii{methods}列表并不存在于对象中，而存放于对象的类中。

\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/object-reference}
  \caption{Instance variables is in the object, but instance methods is in the class}
  \label{fig:object-reference}
\end{figure}

实际上，一个对象主要包括如下几个部分：唯一的\ascii{object id}，一组标记位\ascii{flags}，\ascii{instance variables}，及其\ascii{class}指针。其中，对象的\ascii{class}指针指向对象所属的类，对象的\ascii{methods}列表存在其类中。

也就是说，对象的\ascii{methods}等价于类的\ascii{instance methods}。

\begin{leftbar}
\begin{ruby}
point.class                              # Point
point.methods == Point.instance_methods  # true
\end{ruby}
\end{leftbar}

\subsection{Classes themselves are nothing but objects}

在\ascii{Ruby}中，类是一种特殊的对象，其所属的类为\ascii{Class}。\footnote{\ascii{Class}也是一个对象，其所属类为本身。}

\begin{leftbar}
\begin{ruby}
point.class  # Point
Point.class  # Class
Class.class  # Class
\end{ruby}
\end{leftbar}

因为类是一个对象，也是\ascii{Object}的子类，从而可以调用由\ascii{Object}继承下来的所有\ascii{instance method}。

\begin{leftbar}
\begin{ruby}
Object.kind_of?(Object)    # true
Object.kind_of?(Class)     # true
Class.kind_of?(Object)     # true
Class.kind_of?(Class)      # true
'hey'.kind_of?(String)     # true
String.kind_of?(Class)     # true
\end{ruby}
\end{leftbar}

此外，因为类是\ascii{Class}的对象，所以\ascii{Class}中的\ascii{instance methods}可以被类直接调用。

\begin{leftbar}
\begin{ruby}
Class.instance_methods(false)  # [:allocate, :new, :superclass]
String.new                     # ""
String.allocate                # ""
String.superclass              # Object
\end{ruby}
\end{leftbar}

迭代运用\ascii{Class\#superclass}，可以推出\ascii{Ruby}中最基本的类层次关系。

\begin{leftbar}
\begin{ruby}
Class.superclass       # Module
Module.superclass      # Object
Object.superclass      # BasicObject
BasicObject.superclass # nil
\end{ruby}
\end{leftbar}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/object-model-2}
  \caption{class and superclass}
  \label{fig:object-model-2}
\end{figure}

因为\ascii{Class}是\ascii{Module}的子类，所以任何一个类，同时也是(\ascii{is-a})一个\ascii{Module}。

\end{content}

\section{Method Lookup}

\begin{content}

\subsection{Object Method Lookup Rule}

\ascii{Ruby}查找方法遵循一个最基本的规则：\ascii{one step to the right, then up}

\begin{enum}
  \eitem{\ascii{go one step to the right into the receiver's class}}
  \eitem{\ascii{and then go up the ancestors chain until you ﬁnd the method.}}
\end{enum}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.7\textwidth]{figures/object-method-lookup}
  \caption{Method lookup}
  \label{fig:method-lookup}
\end{figure}

\subsection{Self}

在\ascii{Ruby}中，\ascii{self}总是指向当前对象；\ascii{self}也是\ascii{instance variable}的隐式引用变量；同样，\ascii{self}也是方法的默认\ascii{receiver}。

存在两种场景\ascii{self}会发生变化：
\begin{enum}
  \eitem{\ascii{method call}}
  \eitem{\ascii{class/module deﬁnition}}
\end{enum}

\subsection{Main Object}

在顶级作用域中，\ascii{self}指向\ascii{Object}的\ascii{main}对象。因为\ascii{Object Mixin Kernel}模块，所以在顶级作用域中可以直接调用\ascii{Kernel}模块中的所有\ascii{instance method}。

\begin{leftbar}
\begin{ruby}
self          # main
self.class    # Object
puts "hello, world" # "hello, world"
\end{ruby}
\end{leftbar}

\subsection{Class Instance Variable VS. Class Variable}

\ascii{class variable}为所属该类的所有对象\ascii{instance method}，类及其子类所见。但\ascii{class instance variable}为类对象的\ascii{instance variable}，与普通对象的\ascii{instance variable}一样，仅仅为该对象所见。

\begin{leftbar}
\begin{ruby}
class MyClass
  @@var = 3
  @var  = 2

  def self.class_instance_method
    @var
  end

  def self.class_method
    @@var
  end

  def write()
    @var  = 1
    @@var = 4
  end

  def read()
    @var
  end
end

obj = MyClass.new
obj.write

obj.read                         # 1
MyClass.class_instance_method    # 2
MyClass.class_method             # 4
\end{ruby}
\end{leftbar}

一般地，往往使用\ascii{class instance variable}，而谨慎使用\ascii{class variable}。

\end{content}

\section{Method Visibility}

\begin{content}

\subsection{public, protected, private}

\ascii{public, protected, private}只能修饰\ascii{instance method}。\ascii{instance variable, class instance variable, class variable}默认是\ascii{private}的，但可以通过\ascii{attr-*}对\ascii{instance variable, class instance variable}设置属性器；

\begin{leftbar}
\begin{ruby}
class Point
  @n  = 0

  def initialize(x, y)
    @x = x
    @y = y
    @n += 1
  end

  class << self
    attr_reader :n
  end
end
\end{ruby}
\end{leftbar}

\ascii{constant}默认是public的，没有办法改变这种性质。

\ascii{Ruby}的方法默认是\ascii{public}的，但存在两个例外：
\begin{enum}
  \eitem{\ascii{initialize}总是\ascii{private}}
  \eitem{顶级作用域中的方法默认成为\ascii{main}对象\ascii{private instance method}}
\end{enum}

\ascii{private intance method}只能通过隐式的\ascii{self}调用，与其他语言不同之处。\ascii{private intance method}可以被本类或子类的方法通过\ascii{self}隐式地调用。

\ascii{protected}与\ascii{private}相似，也只能在该类或子类中被调用。不同之处在于，\ascii{protected}还可以被该类或子类的其他实例显示地调用。

\ascii{public, protected, private}其实是\ascii{Module}的\ascii{private instance method}。在类定义体内，\ascii{self}指向类对象本身，而所有类都是\ascii{Class(或Module)}；即使它们像关键字一样被使用，其本质上是在\ascii{self}上被隐式调用的方法而已。

\subsection{Singleton Pattern}

\begin{leftbar}
\begin{ruby}
class Logger
  def initialize
    @log = File.open("proj.log", "a")
  end

  @@instance = Logger.new    

  def self.instance
    return @@instance  
  end

  def log(msg)
    @log.puts(msg)
  end

  private_class_method :new
end
\end{ruby}
\end{leftbar}

\begin{leftbar}
\begin{ruby}
require 'singleton'

class Logger
  include Singleton

  def initialize
    @log = File.open("log.txt", "a")
  end    

  def log(msg)
    @log.puts(msg)
  end
end
\end{ruby}
\end{leftbar}

\begin{leftbar}
\begin{ruby}
module Logger
  def self.log(msg)
    @@log ||= File.open("log.txt", "a")
    @@log.puts(msg)
  end
end
\end{ruby}
\end{leftbar}

\subsection{Static Factory Method}

静态工厂方法是一种非常有用的工具，相对于\ascii{new}，静态工厂方法有时提供了比\ascii{new class method}更加具有意义。

\begin{leftbar}
\begin{ruby}
class Point
  def initialize(x, y)
    @x, @y = x, y
  end

  class << self
    def cartesian(x, y)
      new(x, y)
    end

    def polar(r, theta)
      new(r*Math.cos(theta), r*Math.sin(theta))
    end
  end

  private_class_method :new
end
\end{ruby}
\end{leftbar}

\subsection{Using module\_function}

\end{content}

\section{Eigenclasses}

\begin{content}

\subsection{Singleton Method}

\ascii{singleton method}就是特定于某个\ascii{object}的\ascii{instance method}。存在如下几种方式创建特定\ascii{object}的\ascii{singleton method}。

\begin{leftbar}
\begin{ruby}
snoopy = Dog.new

def snoopy.alter_ego
  "Red Baron"
end

class << snoopy
  def alter_ego
    "Red Baron"
  end
end
\end{ruby}
\end{leftbar}

\ascii{Meaning that we can create methods for a specific dog object that are not shared by other instances of the Dog class.}

\begin{leftbar}
\begin{ruby}
snoopy.alter_ego # "Red Baron"
fido.alter_ego   # NoMethodError
\end{ruby}
\end{leftbar}

\subsection{Singleton Method Lookup}

\ascii{singleton method}方法的查找同样遵循统一的\ascii{method lookup rule}。\ascii{Ruby}为持有\ascii{singleton method}的对象创建一个\ascii{anonymous class}，并称为\ascii{eigenclass}；而原始的类则转变为\ascii{eigenclass}的父类，从而实现了\ascii{method lookup}的统一规则。

\begin{figure}[H]
  \centering
  \includegraphics[width=0.7\textwidth]{figures/eigenclass-method-lookup}
  \caption{Eigenclass method lookup}
  \label{fig:eigenclass-method-lookup}
\end{figure}

\begin{leftbar}
\begin{ruby}
snoopy.alter_ego # "Red Baron"
snoopy.bark      # "woof"
\end{ruby}
\end{leftbar}

\subsection{Class methods are nothing but singleton methods for classes}

类是一种特殊的对象，\ascii{class method}本质上是类对象的\ascii{singleton method}。\ascii{class method}的定义类似于普通对象的\ascii{singleton method}定义，也存在多种形式。

\begin{leftbar}
\begin{ruby}
class Dog
  def self.closest_relative
    "wolf"
  end
end

class Dog
  class << Dog
    def closest_relative
      "wolf"
    end
  end
end

def Dog.closest_relative
  "wolf"
end

class << Dog
  def closest_relative
    "wolf"
  end
end

class Dog
  class << self
    def closest_relative
      "wolf"
    end
  end
end
\end{ruby}
\end{leftbar}

\subsubsection{Class Extension}
通过向类的\ascii{eigenclass}中\ascii{minxin}模块定义类方法。

\begin{leftbar}
\begin{ruby}
class Dog; end

module DogModule
  def closest_relative
    "wolf"
  end
end

class << Dog
  include DogModule
end

Dog.closest_relative     # "wolf"
\end{ruby}
\end{leftbar}

\subsubsection{Class Extension Mixin}

\begin{leftbar}
\begin{ruby}
module Rake

  module PrivateReader

    def self.included(base)
      base.extend(ClassMethods)
    end

    module ClassMethods

      def private_reader(*names)
        attr_reader(*names)
        private(*names)
      end
    end

  end
end
\end{ruby}
\end{leftbar}

类似于\ascii{private\_reader}，常被社区称为类宏，是一种常用的元编程技术。

\begin{leftbar}
\begin{ruby}
require 'rake/private_reader'

module Rake

  class ThreadHistoryDisplay
    include Rake::PrivateReader

    private_reader :stats

    def initialize(stats)
      @stats = stats
    end

    def show
      stats.each do |stat|
      end
    end
  end

end
\end{ruby}
\end{leftbar}

\subsubsection{Class Method Lookup Rule}

对象的\ascii{instance method}通过其类及其继承链中进行查找，对象的\ascii{singleton method}也是通过其\ascii{eigenclass}及其继承链中进行查找。

类是一种特殊的对象，\ascii{class method}是类对象的\ascii{singleton method}，按照同样的规则，\ascii{Ruby}为类对象生成一个\ascii{eigenclass}。

\begin{leftbar}
\begin{ruby}
class Mammal
  def self.warm_blooded?
    true
  end
end
 
class Dog < Mammal
  def self.closest_relative
    "wolf"
  end
end
 
Dog.closest_relative # "wolf"
Dog.warm_blooded?    # true
\end{ruby}
\end{leftbar}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.7\textwidth]{figures/entire-method-lookup}
  \caption{Entire method lookup}
  \label{fig:entire-method-lookup}
\end{figure}

\subsection{How to get eigenclasses?}

\begin{leftbar}
\begin{ruby}
class Object
  def eigenclass
    class << self
      self
    end
  end
end

def point.move_to(x, y)
  @x += x
  @y += y
end

point.eigenclass             # #<Class:#<Point:0x107ae40>>
point.eigenclass.class       # Class
point.eigenclass.superclass  # Point
\end{ruby}
\end{leftbar}

两个\ascii{self}有着微妙的差别。第一个\ascii{self}指向调用\ascii{eigenclass}方法的对象，即\ascii{receiver object}；\ascii{class << self}随后打开\ascii{receiver object}的\ascii{eigenclass}，随后处在\ascii{eigenclass}作用域中，所以第二个\ascii{self}指向\ascii{eigenclass class}对象本身。

\end{content}
